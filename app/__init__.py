from flask import Flask

from config import AppConfig

from api import API

APP = Flask(__name__, template_folder='templates', static_folder='static')
APP.config.from_object(AppConfig())

APP.register_blueprint(API, url_prefix='/api')

from app import routes
