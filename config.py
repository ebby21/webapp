import os
import sys

class AppConfig():
	try:
		# Load ENV VARS here
		SECRET_KEY = os.environ['SECRET_KEY']
	except KeyError:
		print('Environment Variable(s) not set.', file=sys.stderr)
