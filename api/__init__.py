from flask import Blueprint

API = Blueprint(__name__, 'api')

from api import routes
